var express       = require('express');
var router        = express.Router();
var Promise       = require('bluebird');
var environment   = "production";
var data          = require('../public/data/data.json');

function debugprint(data){
  if(environment === "development"){
    console.log(data);
  }
}

/* GET home page. */
router.get('/', function(req, res, next) {
  debugprint(data.skills);
  res.render('index', { title: 'Rick Calder', 'data': data });
});


module.exports = router;