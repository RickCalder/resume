var express         = require('express');
var exphbs          = require('express-handlebars');
var path            = require('path');
var logger          = require('morgan');
var cookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var session         = require('express-session');
var flash           = require('express-flash');
var sassMiddleware  = require('node-sass-middleware');
var path            = require('path');
var srcPath         = __dirname + '/sass';
var destPath        = __dirname + '/public/styles';

var routes          = require('./routes/index');

var app             = express(),
    hbs             = exphbs.create({
                      defaultLayout:'main',
                      helpers: {
                        section: function(name, options){ 
                            if(!this._sections) this._sections = {};
                            this._sections[name] = options.fn(this); 
                            return null;
                        },
                        toLowerCase: function(str){
                          if(str==null)return null;
                          return str.toLowerCase();
                        },
                        testPrint: function(optionalValue){
                          console.log(optionalValue);
                        },
                        versionCompare: function(first, second, options){
                          if(first === second){
                            return options.fn(this);
                          }
                          return options.inverse(this);
                        }
                      }    
                    });
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
// set up sessions
app.use(session({
  secret:'calder12',
  resave: false,
  saveUninitialized: true
}));
app.use(flash());
app.use('/styles',sassMiddleware({
    /* Options */
    src: srcPath,
    dest: destPath,
    debug: true,
    outputStyle: 'compressed'
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req,res,next){
    next();
});
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
